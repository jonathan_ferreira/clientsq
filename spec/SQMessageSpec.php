<?php

namespace spec;

use SQMessage;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SQMessageSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(SQMessage::class);
    }

    function it_has_method_to_string()
    {
        $this->__toString()->shouldReturnString();
    }
}
