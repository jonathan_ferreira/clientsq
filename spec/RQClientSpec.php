<?php

namespace spec;

use RQClient;
use SQMessage;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RQClientSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(RQClient::class);
        $this->initRabbitMQ()->shouldBeAnInstanceOf(RQClient::class);
    }

    function it_can_publish_message(SQMessage $msg)
    {
        $this->publish($msg)->shouldReturn(true);
    }
}
